import base64
import glob
import json
import os
import pathlib
import zlib
from datetime import datetime
from os.path import exists

from celery import Celery
from celery.schedules import crontab
from kombu import Exchange, Queue

CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/jetbrains_clear_evaluation'

celery_broker = Celery('tasks', broker=CELERY_BROKER_URL)

clear_evaluation_exchange = Exchange('clear_evaluation', type='direct')

celery_broker.conf.task_queues = (
    Queue('clear_evaluation', clear_evaluation_exchange),
)

celery_broker.conf.timezone = 'UTC'

ROOT_FILEPATH = pathlib.Path(__file__).parent.as_posix()

FILE_WITH_STATE = f'{ROOT_FILEPATH}/faggot.txt'

TIME_DELAY = 30

HOME_DIRECTORY = '{HOME_DIRECTORY}'


@celery_broker.task
def clear_evaluation():
    if __check_is_time_to():
        __clear_evaluation()


def __clear_evaluation() -> None:
    os.system(f'rm -rf {HOME_DIRECTORY}/.java/.userPrefs/prefs.xml')
    os.system(f'rm -rf {HOME_DIRECTORY}/.java/.userPrefs/jetbrains/prefs.xml')

    for ide_name in ['WebStorm', 'PyCharm']:
        existed_paths = glob.glob(f'{HOME_DIRECTORY}/.config/JetBrains/{ide_name}*/')
        if len(existed_paths) > 0:
            for path in existed_paths:
                os.system(f'rm -rf {path}/eval')
                os.system(f'sed -i -E \'s/<property name=\\"evl.*\\".*\\/>//\' {path}/options/other.xml')
                os.system(f'rm -rf {HOME_DIRECTORY}/.java/.userPrefs/jetbrains/{ide_name.lower()}')


def __check_is_time_to() -> bool:
    if not exists(FILE_WITH_STATE):
        os.system(f'touch {FILE_WITH_STATE}')
        __set_current_date_state()
        return True
    last_clearing_time = ''
    with open(FILE_WITH_STATE, 'r') as file:
        last_clearing_time = file.read().strip(' \t\r\n')
    if last_clearing_time == '':
        __set_current_date_state()
        return True

    datetime_object = datetime.strptime(last_clearing_time, '%m/%d/%y')

    if (datetime.now() - datetime_object).days >= TIME_DELAY:
        __set_current_date_state()
        return True

    return False


def __set_current_date_state():
    with open(FILE_WITH_STATE, 'w') as file:
        file.write(datetime.now().strftime('%m/%d/%y'))


celery_broker.add_periodic_task(schedule=10.0, sig=clear_evaluation.s(),
                                queue='clear_evaluation')
