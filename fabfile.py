import pathlib

from fabric.operations import local
from os.path import exists

ROOT_FILEPATH = pathlib.Path(__file__).parent.as_posix()


CELERY_BEAT_CONF_NAME = 'jetbrains_clearer_service_beat.conf'
CELERY_WORKER_CONF_NAME = 'jetbrains_clearer_service_worker.conf'

CELERY_BEAT_LOCAL_CONF_NAME = 'supervisor_service_beat.conf'
CELERY_WORKER_LOCAL_CONF_NAME = 'supervisor_service_worker.conf'

SUPERVISOR_CONF_FILES = [
    (CELERY_BEAT_LOCAL_CONF_NAME, CELERY_BEAT_CONF_NAME),
    (CELERY_WORKER_LOCAL_CONF_NAME, CELERY_WORKER_CONF_NAME),
]

CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672/jetbrains_clear_evaluation'


def deploy():
    __setup_supervisor()


def __setup_supervisor():
    local('rabbitmqctl add_vhost jetbrains_clear_evaluation')
    local('rabbitmqctl set_permissions -p jetbrains_clear_evaluation guest ".*" ".*" "."')
    if not exists(f'{ROOT_FILEPATH}/logs'):
        local(f'mkdir {ROOT_FILEPATH}/logs')
    for local_conf, server_conf in SUPERVISOR_CONF_FILES:
        supervisor_filepath = f'/etc/supervisor/conf.d/{server_conf}'
        local(f'cp {ROOT_FILEPATH}/{local_conf} {supervisor_filepath}')
        local(f'sed -i \'s+SERVICE_DIRECTORY+{ROOT_FILEPATH}+g\' {supervisor_filepath}')
        local(f'sed -i \'s+CELERY_BROKER_URL+{CELERY_BROKER_URL}+g\' {supervisor_filepath}')

    local('supervisorctl reread && supervisorctl reload')
